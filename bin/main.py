import sys
print ('Running Inspector Gadget version 02/2019')
print (sys.version)

import time
import glob
import os
import subprocess
from monitor import monitor
from os import listdir
from os.path import isfile, join
import shutil
import json
import hashlib

# import matplotlib.pyplot as plt
# print(plt.gcf().canvas.get_supported_filetypes())

# EXPERIMENT CONFIGURATION #
interval   = 0.1
samples    = 300
tag        = "TRICORDER_TAG"
executions = 50 if tag == "REFERENCE" else 30

# INTERNAL CONFIGURATION #
home = '/opt/tricorder'
print('home path: '+ home)

exefiles        = [os.path.join(home,"bin","server")]
config_file_all = os.path.join(home,"defs",'defs_all.json')
config_file     = os.path.join(home,'defs.json')
output_path     = os.path.join(home,'output')
with open(config_file_all, 'r') as jsonFile:
	jvalues = json.load(jsonFile)
	jsonFile.close()

shutil.copyfile(config_file_all,os.path.join(output_path,'/defs_all.json'))

for jcon in jvalues['configurations']:

	experiment_tag = jcon['tag']
	
	prefix = experiment_tag

	print("Configuration: "+ prefix)

	with open(config_file, 'w') as jsonFile:
		json.dump(jcon,jsonFile)
		jsonFile.close()

	shutil.copyfile(config_file,os.path.join(output_path,"output",prefix+'defs.json'))

	prepare_exe = os.path.join(home,"bin",'workloadgenerator')
	proc_prepare = subprocess.Popen([prepare_exe,config_file])
	proc_prepare.wait()

	for sut_server in exefiles[:1]:
		#sut_server = sut_server.replace('\\','/')
		sut_server_file = os.path.basename(sut_server)
		sut_client = os.path.join(home,"bin",'client')
		print ('SUTs: ' + sut_server + ' ' + sut_client)

		for i in range(executions):
			print ("execution " + str(i) + " of " + str(executions))
			proc_server = subprocess.Popen([sut_server,config_file])
			m = monitor(sut_server_file,prefix,output_path,interval,samples,False,True)
			proc_client = subprocess.Popen([sut_client,config_file])
			m.get_samples()
			m.dump_csv(i)
			proc_server.kill()
			proc_client.kill()
			proc_client.wait()
			proc_server.wait()

			BLOCKSIZE = 65536
			bin_hash = ''
			csv_hash = ''

			hasher = hashlib.sha1()
			try:
				with open(home + '/output/output/bin/'+sut_server_file+'.dump.bin', 'rb') as afile:
					buf = afile.read(BLOCKSIZE)
					while len(buf) > 0:
						hasher.update(buf)
						buf = afile.read(BLOCKSIZE)
				bin_hash = hasher.hexdigest()
				print(bin_hash)
			except:
				print ('no bin output')

			hasher = hashlib.sha1()
			try:
				with open(home + '/output/output/csv/'+sut_server_file+'42.dump.csv', 'rb') as afile:
					buf = afile.read(BLOCKSIZE)
					while len(buf) > 0:
						hasher.update(buf)
						buf = afile.read(BLOCKSIZE)
				csv_hash = hasher.hexdigest()
				print(csv_hash)
			except:
				print ('no csv output')

			with open(output_path + '/' + prefix + '.bin.hash', 'a') as afile:
				afile.write(sut_server_file+':\t'+bin_hash+'\n')
			with open(output_path + '/' + prefix + '.csv.hash', 'a') as afile:
				afile.write(sut_server_file+':\t'+csv_hash+'\n')

			print ("done execution " + str(i))
	print("Finished config file")
print("Finished")

