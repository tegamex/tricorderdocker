import time
import psutil
import time
import datetime
import h5py
import os.path
import numpy
import csv
import matplotlib.pyplot as plt

class monitor:

	def __init__(self,sut,prefix,output_path,interval=0.2,sample_count=100, onefileconcat = False, show_plot=False):

		self.sut = sut
		self.prefix = prefix
		self.output_path = output_path
		self.interval = interval
		self.sample_count = sample_count
		self.onefile = onefileconcat
		self.show_plot = show_plot
		
		self.cpu_data = []
		self.mem_data = []		
		self.io_data = []
		self.last_io = 0

		pid = -1
		while pid == -1:
			for proc in psutil.process_iter():
				if proc.name() == sut:
					pid = proc.pid
		print(pid)
		if (pid != -1):
			self.date = str(datetime.datetime.now())
			self.pid = pid
			self.proc = psutil.Process(pid)
			self.proc_name = self.proc.name()
		else:
			print ('Process not found')

	def get_sample_line(self):

		p = psutil.Process(self.pid)
		
		# the first line is blocking, being responsible for the sampling interval
		raw_cpu = p.cpu_percent(self.interval)
		raw_mem = p.memory_info().rss
		sample_io = p.io_counters()
		raw_io_diff =  (sample_io.read_count + sample_io.write_count) - self.last_io
		self.last_io = sample_io.read_count + sample_io.write_count

		return raw_cpu,raw_mem,raw_io_diff

	def get_samples(self):
		try:
			if (self.show_plot == True):
				plt.axis([0, self.sample_count, 0, 1000])

			# waste the first acquisition to get valid normalize values
			raw_cpu,raw_mem,raw_io_diff = self.get_sample_line()
			for i in range(0,self.sample_count):
				raw_cpu,raw_mem,raw_io_diff = self.get_sample_line()
				self.cpu_data.append(raw_cpu)
				self.mem_data.append(raw_mem)
				self.io_data.append(raw_io_diff)

				if (self.show_plot == True):
					norm_mem = [x / 1000000.0 for x in self.mem_data]
					norm_io = [x / 10.0 for x in self.io_data]
					plt.plot(self.cpu_data, color='b', label='cpu')
					plt.plot(norm_mem, color='g', label='mem')
					plt.plot(norm_io, color='r', label='io')
					if (i == 0):
						plt.legend()
		except:
			print ("Process closed")

	def dump_csv(self, num):

		if (self.show_plot == True):
				pname = self.proc_name.replace('.','_')
				plt.savefig(self.output_path+'/'+self.prefix+pname+'_'+str(num)+".png")
				plt.close()

		if (self.onefile == False):
			fname = self.proc_name + '_' + str(num) + '.csv'			
			mode = 'w'
		else:
			fname = self.proc_name + '.csv'
			mode = 'a'

		fname = self.prefix + '_' + fname
		fname = fname.replace('.','_')
		self.output_file_name = self.output_path + '/' + fname
		with open(self.output_file_name, mode) as csvfile:
			spamwriter = csv.writer(csvfile, delimiter=';',quotechar='|', quoting=csv.QUOTE_MINIMAL)
			for row in zip(self.cpu_data,self.mem_data,self.io_data): # no net data for now
				spamwriter.writerow(row)



