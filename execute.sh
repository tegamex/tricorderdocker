
if [ -z "${TRICORDER_TAG}" ]; then
  TRICORDER_TAG="REFERENCE"
fi 

export TRICORDER_TAG

echo "MUTANTE $TRICORDER_TAG"

git clone https://gitlab.com/vmontes/networktelemetry.git

cd networktelemetry

git checkout 70d20c7e

sed -i 's/__declspec(dllexport)/Q_DECL_EXPORT/g' src/dataprocessinglib/dataprocessor.h
sed -i 's/__declspec(dllexport)/Q_DECL_EXPORT/g' src/dataprocessinglib/binprocessor.h
sed -i 's/__declspec(dllexport)/Q_DECL_EXPORT/g' src/dataprocessinglib/csvprocessor.h
sed -i 's;#include "common/pkt.h";#include "dataprocessinglib_global.h"\n#include "common/pkt.h";g' src/dataprocessinglib/dataprocessor.h
sed -i 's;#include "common/pkt.h";#include "dataprocessinglib_global.h"\n#include "common/pkt.h";g' src/dataprocessinglib/binprocessor.h
sed -i 's;#include "common/pkt.h";#include "dataprocessinglib_global.h"\n#include "common/pkt.h";g' src/dataprocessinglib/csvprocessor.h

case $TRICORDER_TAG in
    BCLEAN)
        TRICORDER_FLAG="FAULT_BINPKTPROCESSOR_PROCESS_CLEANUP"
        ;;
    INFINITE)
        TRICORDER_FLAG="FAULT_CSVPKTPROCESSOR_PROCESS_INFINITE"
        ;;
    MONO)
        TRICORDER_FLAG="FAULT_DATAPROCESSOR_MONOLITHIC"
        ;;
    SLEEP)
        TRICORDER_FLAG="FAULT_BINPKTPROCESSOR_PROCESS_SLEEP"
        ;;
    SWAP)
        TRICORDER_FLAG="FAULT_NETWORK_PKT_WINDOWSIZE_ACQNUM_SWAP"
        ;;
    UNLOCK)
        TRICORDER_FLAG="FAULT_PROCESSOR_BUSY_UNLOCK"
        ;;
    NOBREAK)
        TRICORDER_FLAG="FAULT_DATAPROCESSOR_RUN_NO_BREAK"
        ;;
    CONTROL)
        TRICORDER_FLAG="NO_FAULT"
        ;;
esac

echo "FLAG $TRICORDER_FLAG"

sed -i "s;1234;50035;g" src/client/pktsendercontrol.cpp
sed -i "s;1234;50035;g" src/server/dataserver.cpp

rm -f /usr/lib/x86_64-linux-gnu/libdataprocessing.so
rm -f /usr/lib/x86_64-linux-gnu/libdataprocessing.so.1
qmake linux/dataprocessing.pro DEFINES+="$TRICORDER_FLAG"
make clean
make
ln -s $PWD/libdataprocessing.so /usr/lib/x86_64-linux-gnu/libdataprocessing.so
ln -s $PWD/libdataprocessing.so /usr/lib/x86_64-linux-gnu/libdataprocessing.so.1

qmake linux/workloadgenerator.pro
make clean
make

qmake linux/client.pro
make clean
make

qmake linux/server.pro
make clean
make

cd ..

cp -r networktelemetry/windows/data/ defs
cp networktelemetry/server networktelemetry/client networktelemetry/workloadgenerator bin

find defs -type f -exec sed -i 's;\\;\/;g' {} \;
find defs -type f -exec sed -i 's;//;/;g' {} \;
find defs -type f -exec sed -i "s;c:/datalab/network;$PWD/output;gi" {} \;
sed -i "s;TRICORDER_TAG;$TRICORDER_TAG;g" bin/main.py




