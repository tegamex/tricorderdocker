
if [ -z "${TRICORDER_TAG}" ]; then
  echo "must set TRICORDER_TAG environment value"
  exit 1
fi 
rm -Rf output
mkdir output
sed -i "s;REFERENCE;$TRICORDER_TAG;g" Dockerfile
sudo docker build -t vmontes . --network=host
sudo docker run --mount type=bind,source="$(pwd)"/output,target=/opt/tricorder/output vmontes

tar -zcvf $TRICORDER_TAG.tar.gz nohup.out output

sudo poweroff
