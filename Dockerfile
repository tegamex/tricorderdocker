FROM debian:bookworm-slim

WORKDIR /opt/tricorder

RUN apt-get update && apt-get install -qq libqt5core5a libqt5network5 build-essential qt5-qmake git qtbase5-dev git python2 python-pip python-tk 

RUN pip2 install psutil h5py matplotlib

RUN mkdir -p /opt/tricorder

ENV TRICORDER_TAG=REFERENCE

COPY ./execute.sh /opt/tricorder

COPY ./bin /opt/tricorder/bin

RUN /bin/sh /opt/tricorder/execute.sh

CMD ["/bin/sh","/opt/tricorder/bin/execute.sh"]
